package ru.unn.iterationtask_scala

import java.io.File

import breeze.linalg._

object Main {
  def main(args: Array[String]): Unit = {
    val aMat:DenseMatrix[Double] = csvread(new File("a.csv"))
    print(s"\nMatrix A loaded:\n $aMat")
    val bMat:DenseMatrix[Double] = csvread(new File("b.csv"))
    print(s"\nMatrix B loaded:\n $bMat")
    print("\nCalculation started")
    val start = System.currentTimeMillis()
    val str = (0 to 1023)
      .toStream.map((number)=>{
      val vector:DenseVector[Double] = generate(number)
      val scalar = aMat * vector + bMat*vector.map((value)=>1-value)
      (vector,scalar.data.toStream.sum)
    }).maxBy((v)=>v._2)
    val elapsedTime = System.currentTimeMillis()-start
    print(s"\nCalculation finished in $elapsedTime ms")
    print(s"\nOptimal vector and optimum: $str")
  }

  def generate(number: Int):DenseVector[Double] = {
    DenseVector((0 to 9).toStream.map((num)=>1.0*(number>>num&1)).reverse.toArray)
  }
}
