package ru.unn.iterationtask;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.math3.linear.*;
import ru.unn.iterationtask.implementation.*;


public class Main {
    private final static String AFILE = "a.csv";
    private final static String BFILE = "b.csv";
    private final static RealMatrixFormat FORMAT =
            new RealMatrixFormat("","","|","|","\n","  ");


    public static void main(String[] args) {
        RealMatrix aMat = MatrixDAO.getMatrix(AFILE);
        if(aMat== null) return;
        System.out.println("\nMatrix A loaded:\n");
        System.out.println(FORMAT.format(aMat));
        RealMatrix bMat = MatrixDAO.getMatrix(BFILE);
        if(bMat== null) return;
        System.out.println("\nMatrix B loaded:\n");
        System.out.println(FORMAT.format(bMat));
        System.out.println("\nCalculating by iterating through vectors;");
        Long time = System.nanoTime();
        Pair pair = new CalculatorIterating(aMat,bMat).calculateTask();
        System.out.println("Calculation took "+ (System.nanoTime()-time) +
                " nanoseconds.\nResults:");
        System.out.println("Vector: " + pair.getLeft());
        System.out.println("Optimum: " + pair.getRight());
        System.out.println("\nCalculating by checking coefficients;");
        time = System.nanoTime();
        pair = new CalculatorOptimised(aMat,bMat).calculateTask();
        System.out.println("Calculation took "+ (System.nanoTime()-time) +
                " nanoseconds.\nResults:");
        System.out.println("Vector: " + pair.getLeft());
        System.out.println("Optimum: " + pair.getRight());
        System.out.println("\nCalculating by branching;");
        time = System.nanoTime();
        pair = new CalculatorBranching(aMat,bMat).calculateTask();
        System.out.println("Calculation took "+ (System.nanoTime()-time) +
                " nanoseconds.\nResults:");
        System.out.println("Vector: " + pair.getLeft());
        System.out.println("Optimum: " + pair.getRight());
        System.out.println("\nCalculating by branching and cutting;");
        time = System.nanoTime();
        pair = new CalculatorCut(aMat,bMat).calculateTask();
        System.out.println("Calculation took "+ (System.nanoTime()-time) +
                " nanoseconds.\nResults:");
        System.out.println("Vector: " + pair.getLeft());
        System.out.println("Optimum: " + pair.getRight());
        System.out.println("\nCalculating by binary search;");
        time = System.nanoTime();
        pair = new CalculatorSearching(aMat,bMat).calculateTask();
        System.out.println("Calculation took "+ (System.nanoTime()-time) +
                " nanoseconds.\nResults:");
        System.out.println("Vector: " + pair.getLeft());
        System.out.println("Optimum: " + pair.getRight());
    }


}
