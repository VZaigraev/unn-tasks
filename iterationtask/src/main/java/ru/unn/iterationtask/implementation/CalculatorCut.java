package ru.unn.iterationtask.implementation;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;


public class CalculatorCut extends CalculatorAbstract {
    private Double record = -Double.MAX_VALUE;
    public CalculatorCut(RealMatrix A, RealMatrix B) {
        super(A, B);
    }

    @Override
    public Pair<RealVector, Double> calculateTask() {
        Pair<RealVector, Double> pair = calculateIndividual(new ArrayRealVector(dimension,0.5d),0);
        return Pair.of(pair.getKey(),pair.getValue()+scalarValue);
    }

    private Pair<RealVector,Double> calculateIndividual(RealVector vector,int level) {
        Double suprenum = vector.dotProduct(vecValue);
        if (suprenum >= record && level < dimension) {
            record = suprenum;
            RealVector vector1 = vector.copy();
            vector.setEntry(level,1d);
            vector1.setEntry(level,0d);
            Pair<RealVector,Double> left = calculateIndividual(vector,level+1);
            Pair<RealVector,Double> right = calculateIndividual(vector1, level+1);
            return left.getValue()>right.getValue()? left : right;
        } else
            return Pair.of(vector,suprenum);
    }

}
