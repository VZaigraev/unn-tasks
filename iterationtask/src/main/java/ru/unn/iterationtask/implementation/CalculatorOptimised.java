package ru.unn.iterationtask.implementation;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import ru.unn.iterationtask.Calculator;


//Based on idea of changing the task: yT*A*(1) + (1)T*B*((1) - y) = yT*A*(1) + (1)T*B*(1) - (1)T*B*y (=)
// a= A*(1) b=(1)T*B ->  (=) (1)T*B*(1) + y x (a-b)
// x - scalar product operation
// (1) - vector of ones

public class CalculatorOptimised extends CalculatorAbstract {


    public CalculatorOptimised(RealMatrix A, RealMatrix B) {
        super(A, B);
    }

    @Override
    public Pair<RealVector, Double> calculateTask() {
        RealVector optimumVector = new ArrayRealVector(dimension,0d);
        for (int i = 0; i < dimension; i++) {
            if(vecValue.getEntry(i)>=0)optimumVector.setEntry(i,1d);
        }
        return Pair.of(optimumVector,optimumVector.dotProduct(vecValue)+scalarValue);
    }
}
