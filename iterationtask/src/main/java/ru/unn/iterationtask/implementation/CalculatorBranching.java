package ru.unn.iterationtask.implementation;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import ru.unn.iterationtask.Calculator;

public class CalculatorBranching extends CalculatorAbstract{


    public CalculatorBranching(RealMatrix A, RealMatrix B) {
        super(A, B);
    }

    @Override
    public Pair<RealVector, Double> calculateTask() {
        RealVector startingVector = new ArrayRealVector(dimension,1d);
        Pair<RealVector,Double> result = calculateIndividually(startingVector,0);
        return Pair.of(result.getKey(),result.getValue()+scalarValue);
    }

    private Pair<RealVector, Double> calculateIndividually(RealVector vector, int level) {
        if(level<dimension) {
            RealVector copy = vector.copy();
            copy.setEntry(level, 0d);
            Pair<RealVector,Double> left = calculateIndividually(vector, level + 1);
            Pair<RealVector,Double> right = calculateIndividually(copy, level + 1);
            return left.getRight() > right.getRight() ? left:right;
        } else {
            return Pair.of(vector,vector.dotProduct(vecValue));
        }
    }
}
