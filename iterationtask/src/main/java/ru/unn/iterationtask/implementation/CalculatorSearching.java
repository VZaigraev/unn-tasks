package ru.unn.iterationtask.implementation;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

public class CalculatorSearching extends CalculatorAbstract {

    public CalculatorSearching(RealMatrix A, RealMatrix B) {
        super(A, B);
    }

    @Override
    public Pair<RealVector, Double> calculateTask() {
        RealVector startingVector = new ArrayRealVector(dimension,1d);
        Pair<RealVector,Double> result = calculateIndividually(startingVector,0);
        return Pair.of(result.getKey(),result.getValue()+scalarValue);
    }

    private Pair<RealVector, Double> calculateIndividually(RealVector vector, int level) {
        if(level<dimension) {
            if (vecValue.getEntry(level)<0) vector.setEntry(level,0d);
            return calculateIndividually(vector,level + 1);
        } else {
            return Pair.of(vector,vector.dotProduct(vecValue));
        }
    }
}
