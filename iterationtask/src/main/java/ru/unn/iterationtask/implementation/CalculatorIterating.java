package ru.unn.iterationtask.implementation;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import ru.unn.iterationtask.Calculator;


import java.util.*;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

public class CalculatorIterating extends CalculatorAbstract {


    public CalculatorIterating(RealMatrix A, RealMatrix B) {
        super(A, B);
    }

    public Pair<RealVector,Double> calculateTask() {
        return Stream.iterate(0L,(a)->a+1)
                .limit(Double.valueOf(Math.pow(2,dimension)).intValue())
                .map(this::calculateIndividual)
                .max(Comparator.comparingDouble(Pair::getRight)).orElse(null);
    }

    public Pair<RealVector,Double> calculateTaskParallel() {
        Pair<RealVector,Double> result =  Stream.iterate(0L,(a)->a+1)
                .parallel()
                .limit(Double.valueOf(Math.pow(2,dimension)).intValue())
                .map(this::calculateIndividual)
                .max(Comparator.comparingDouble(Pair::getRight)).orElse(null);
        return Pair.of(result.getKey(),result.getValue()+scalarValue);
    }

    private Pair<RealVector,Double> calculateIndividual(Long value) {
        RealVector vector = new ArrayRealVector();
        for(int i = dimension-1;i>=0;i--){
            vector = vector.append((value>>i)&1);
        }
        return Pair.of(vector,
                vector.dotProduct(vecValue));
    }
}
