package ru.unn.iterationtask.implementation;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import ru.unn.iterationtask.Calculator;

abstract class CalculatorAbstract implements Calculator {
    final RealVector vecValue;
    final int dimension;
    final Double scalarValue;
    CalculatorAbstract(RealMatrix A, RealMatrix B) {
        if(!A.isSquare()||
                !B.isSquare()||
                !A.transpose().equals(A)||
                !B.transpose().equals(B)) throw new IllegalArgumentException("Matrix must be symmetric");
        if(A.getRowDimension()!=B.getRowDimension()) throw new IllegalArgumentException("A size doesn't equal B size");
        dimension = A.getColumnDimension();
        RealVector ones = new ArrayRealVector(dimension,1d);
        RealVector vecB = B.preMultiply(ones);
        vecValue = A.operate(ones).subtract(vecB);
        scalarValue = vecB.dotProduct(ones);
    }
}
