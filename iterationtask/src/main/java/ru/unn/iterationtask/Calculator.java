package ru.unn.iterationtask;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.math3.linear.RealVector;

public interface Calculator {
    Pair<RealVector,Double> calculateTask();
}
