package ru.unn.iterationtask;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public abstract class MatrixDAO {
    public static RealMatrix getMatrix(String file){
        RealMatrix matrix = null;
        try (CSVReader reader = new CSVReader(new FileReader(file))){
            List<String[]> list = reader.readAll();
            double[][] values = new double[list.size()][list.size()];
            for(int i = 0; i<list.size();i++) {
                String[] line = list.get(i);
                for( int j = 0; j<list.size();j++ ){
                    values[i][j] = Double.valueOf(line[j]);
                    if(Double.isNaN(values[i][j]) || (values[i][j]%1)!=0)
                        throw new NumberFormatException();
                }
            }
            matrix = new Array2DRowRealMatrix(values);
            if(!matrix.equals(matrix.transpose()))
                throw new IllegalArgumentException();
        }
        catch (NumberFormatException e){
            System.out.println("Input has thrown an exception. " +
                    "Matrix should be square and include only Integer elements.");
            matrix = null;
        }
        catch (IllegalArgumentException e) {
            System.out.println("Input has thrown an exception. " +
                    "Matrix should be symmetric.");
            matrix = null;
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return matrix;
    }

    public static void generateMatrix(String file,int size,int seed)  {
        double [][] doubleValues = generateMatrix(size,seed).getData();
        List<String[]> strings = new ArrayList<>();
        for(int i = 0; i<size;i++) {
            strings.add(i,new String[size]);
            for (int j = 0; j < size; j++)
                strings.get(i)[j]=Double.toString(doubleValues[i][j]);
        }
        try(CSVWriter writer = new CSVWriter(new FileWriter(file))) {
            writer.writeAll(strings);
        }  catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static RealMatrix generateMatrix(int size, int seed)  {
        double [][] doubleValues = new double[size][size];
        Random random = new Random(seed);
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                doubleValues[i][j] = random.nextInt(200)-100;
            }
        }
        RealMatrix matrix = new Array2DRowRealMatrix(doubleValues);
        return matrix.add(matrix.transpose());
    }
}
