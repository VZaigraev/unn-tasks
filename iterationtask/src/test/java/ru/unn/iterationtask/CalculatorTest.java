package ru.unn.iterationtask;

import org.apache.commons.math3.linear.RealMatrix;
import org.junit.Assert;
import org.junit.Test;
import ru.unn.iterationtask.implementation.CalculatorBranching;
import ru.unn.iterationtask.implementation.CalculatorCut;
import ru.unn.iterationtask.implementation.CalculatorIterating;
import ru.unn.iterationtask.implementation.CalculatorOptimised;

public class CalculatorTest extends Assert {

    //Testing current implementation, comparing with pre-calculated values.

    @Test
    public void test2() throws Exception {
        RealMatrix A = MatrixDAO.generateMatrix(2,5);
        RealMatrix B = MatrixDAO.generateMatrix(2,15);
        long actual =  new CalculatorCut(A,B).calculateTask().getRight().longValue();
        assertEquals(36,actual);
    }

    @Test
    public void test3() throws Exception {
        RealMatrix A = MatrixDAO.generateMatrix(3,0);
        RealMatrix B = MatrixDAO.generateMatrix(3,10);
        long actual = new CalculatorCut(A,B).calculateTask().getRight().longValue();
        assertEquals(421,actual);
    }

}
